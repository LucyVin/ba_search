from setuptools import setup

setup(
    name = 'beer_search',
    py_modules = ['scrape'],
    install_requires = [
        'Requests',
        'bs4'
        ],
    entry_points = {
        'console_scripts':[
            'beer = scrape:main'
            ]
    },
    version = '0.0.1',
    author = 'lvinc'
    )
