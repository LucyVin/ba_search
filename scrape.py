import re
from sys import argv
import requests
from bs4 import BeautifulSoup

search_string = 'https://www.beeradvocate.com/search/?q='
search_mode = 'beer'
base_url = 'https://www.beeradvocate.com'

def search_str(name, mode='beer'):
    return search_string + name.replace(' ','+') + '&qt=' + mode

def get_beer(beer_url):
    r = requests.get(beer_url)
    soup = BeautifulSoup(r.content, 'html.parser')
    
    title_box = soup.find('div', {'class':'titleBar'})
    beer = title_box.find('h1')
    beer.find('span').extract()
    beer = beer.text

    score = soup.find('span', {'class':'ba-ravg'}).text
    bro_score = soup.find('span', {'class':'ba-bro_score'}).text
    ratings = soup.find('span', {'class':'ba-ratings'}).text

    bro_score = bro_score.replace('\n', '')

    info_box = soup.find('div', {'id':'info_box'})
    content = info_box.stripped_strings
    content = [c for c in content]
    """ get rid of punctuation and header """
    content = content[1:]
    content.pop()

    brewery_block = content[content.index('Brewed by:')+1:content.index('Style:')]
    brewery = brewery_block.pop(0)
    website = brewery_block.pop()
    region = ''.join(brewery_block)
    region = region.replace(',', ', ')

    beer_data = {
        'Beer' : beer,
        'Brewery' : brewery,
        'Region' : region,
        'Website' : website,
        'Style' : content[content.index('Style:')+1],
        'ABV' : content[content.index('Alcohol by volume (ABV):')+1],
        'Availability' : content[content.index('Availability:')+1],
        'Notes' : content[content.index('Notes / Commercial Description:')+1],
        'Score' : score,
        'BroScore':bro_score,
        'Ratings' : ratings
        }
    
    return beer_data
    

def search_beer(name, return_one=True):
    r = requests.get(search_str(name, 'beer'))

    soup = BeautifulSoup(
        r.content,
        'html.parser'
        )

    re_switch = {
        'beer':'^/beer/profile/\d+/\d+/$',
        'place':'^/beer/profile/\d+/$'
        }

    search = soup.find('div', {'id':'ba-content'}).findChildren('li')
    result = []
    """
    beer links are formatted as /beer/profile/123/123
    place links are formatted as /beer/profile/123
    """
    reg = '/beer/profile'
    results = [r for r in search if r.a and bool(re.search(reg, r.a['href']))]
    for res in results:
        beer = res.find('a', href=re.compile(re_switch['beer']))
        beer_name = beer.b.text
        beer_id = beer['href'].replace('/beer/profile/', '')
        brewery = res.find('a', href=re.compile(re_switch['place'])).text

        detail_str = {
            'Beer':beer_name,
            'Brewery':brewery,
            'URL':base_url + beer['href']
            }

        result.append(detail_str)
        
        if return_one:
            return result
   
    return result

def main():
    arg_list = [
        '--url',
        '--all',
        '--detail'
        ]

    url = False
    detail = False
    return_one = True

    if '--url' in argv:
        url = True

    if '--all' in argv:
        return_one = False

    if '--detail' in argv:
        detail = True

    args = []
    for arg in argv[1:]:
        if arg in arg_list:
            continue
        if arg[:2] == '--':
            print('No such argument {}'.format(arg))
            return None

        args.append(arg)

    if detail and not return_one:
        print("Warning: Using --all and --detail together may lead rate limiting")

    if detail and url:
        raise Exception("Can not use --url and --detail simultaneously")

    query_str = ' '.join(args)

    results = search_beer(query_str, return_one)

    msg = "No results found for '{}'.".format(query_str)
    
    if url:
        urls = [beer['URL'] for beer in results]
        msg = '\n'.join(urls)
    
    
    else:
        if detail:
            results = [get_beer(beer['URL']) for beer in results] 

        msg = []
        for beer in results:
            if detail:
                beer['Bro Score'] = beer.pop('BroScore')        
            _msg = ['{}: {}'.format(k, v) for k, v in beer.items()]
            msg += _msg
            msg.append('\n')
        
        if msg[-1] == '\n':
            msg.pop()
        msg = '\n'.join(msg)

    print(msg)
