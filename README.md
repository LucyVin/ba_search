# Beer-Search

A small beerAdvocate search tool, simply grabs the first result from a query.

A small step in the long road to figuring out how the hell I can parse the rest of their site.

I mostly just wanted this for a beer.py script for weechat.

### Installation:

```bash
$ git clone https://gitlab.com/LucyVin/ba_search
$ cd ba_search
$ pip3 install --user .
```

### Usage:

```bash
$ beer Fresh Squeezed
=> Beer: Fresh Squeezed IPA
=> Brewery: Deschutes Brewery
=> URL: https://www.beeradvocate.com/beer/profile/63/60330/

# use --url to return ONLY beer advocate URL. Can be used in conjunction with --all
$ beer --url Hitachino Nest
=> https://www.beeradvocate.com/beer/profile/697/2013/

$ beer --detail Deschutes The Abyss
=> Beer: The Abyss
=> Brewery: Deschutes Brewery
=> Region: Oregon, United States
=> Website: deschutesbrewery.com
=> Style: American Double / Imperial Stout
=> ABV: 12.20%
=> Availability: Winter
=> Notes: Aged in bourbon, Oregon oak, and Pinot Noir barrels.
=> Score: 4.47
=> Ratings: 7,218
=> Bro Score: 4.13 
```

### Options

- `--all` - include all results
- `--detail` - include brewery in results line
- `--url` - only output URL
